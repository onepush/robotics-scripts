function geoJ = geometricJacobian(DHParams, q)
% geoJ = geometricJacobian() takes as inputs:
%   -DHParams: a nx4-matrix where the i-th row contains the following DH
%              parameters of link i: [alpha a d theta] - sorted like this!
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
% and outputs:
%   -geoJ: The resulting geometric jacobian

    [T, A] = dhMatrix(DHParams);
    n_dof = length(q);
    
    sequence = '';
    sequence(1:n_dof) = 'r';
    for index = 1:n_dof
        el = DHParams(index, end);
        if ~ismember(el, q)
            sequence(index) = 'p';
        end
    end
    
    f_r = T(1:3, 4);
    
    Jl = jacobian(f_r, q);
    
    Ja = sym(zeros(3, n_dof));
    
    cells = cell(1, n_dof);
    cells{1} = A{1}(1:3, 1:3);
    for i = 2:n_dof
        cells{i} = cells{i-1}*A{i}(1:3, 1:3);
    end
    
    for i = 1:n_dof
        c = sequence(i);
        
        if c ~= 'r' && c ~= 'p'
            disp("Sequence must be composed of only 'r's and 'p's")
            geoJ = -1;
            return
        end
        
        if c == 'r'
            if i == 1
                Ja(:, i) = [0; 0; 1];
            else
                Ja(:, i) = cells{i-1}*[0; 0; 1];
            end
        end
    end

    geoJ = simplify([Jl; Ja]);

end