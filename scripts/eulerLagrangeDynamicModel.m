function [M, c, g] = eulerLagrangeDynamicModel(DHParams, com_pos, q, dq, vertical_axis, varargin)
% [M, c, g] = eulerLagrangeDynamicModel(DHParams, com_pos, q, dq, 
%                  vertical_axis, varargin) takes as inputs:
%   -DHParams: a nx4-matrix where the i-th row contains the following DH
%              parameters of link i: [alpha a d theta] - sorted like this!
%   -com_pos: a matrix of dimension nx3, where every i-th row is the 
%             transpose of the position of the CoM of link i w.r.t. frame i
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
%   -dq: The n-vector containing the symbolic expressions of the robot's
%        joints' velocity variables
%   -vertical_axis: 
%        Integer which must belong to {1, 2, 3, -1, -2, -3} and
%           which defines which is the vertical axis according to
%           the base frame of the robot, in particular:
%               1: The positive x
%               2: The positive y
%               3: The positive z
%               -1: The negative x
%               -2: The negative y
%               -3: The negative z
%         Note: This works only if the base frame is "standard"
%               in the sense that it does not appear rotated and has
%               2 axes orthogonal to the gravity force.
%   -varargin: accounts for optional parametrs as:
%       -com_mot_pos[Default: []]: nx3-matrix where the i-th row contains
%                                  the position of the i-th motor w.r.t
%                                  Oi-1
%       -rot_mot[Default: zeros(n_dof, 3)]: nx3-matrix with the i-th row 
%                              containing 3 elements: alpha_i, beta_i and 
%                              gamma_iencoding an euler rotation through 
%                              the 'xyz' sequence which represents the 
%                              orientation of motor i w.r.t. to frame i-1
%       -reduction_ratios[Default: []]: the n-vector containing as element
%                                       i the reduction ratio between
%                                       theta_i and q_i. These are not used
%                                       if theta ~= []
%       -theta[Default: []]: the n-vector containing the symbolic  
%                            expressions of the robot motors' position
%                            variables. If not empty the robot will be
%                            assumed to be elastic.
%                            If the robot has only some elastic links 
%                            (this is not tested but) it might work with 
%                            dtheta = [dq1 ... dqi-1 dthetai dqi ... dq]
%                            where the only elastic link is assumed to be
%                            the i-th.
%       -dtheta[Default: []]: the n-vector containing the symbolic  
%                             expressions of the robot motors' velocity
%                             variables. If not empty the robot will be
%                             assumed to be elastic.
%                             If the robot has only some elastic links 
%                             (this is not tested but) it might work with 
%                             dtheta = [dq1 ... dqi-1 dthetai dqi ... dq]
%                             where the only elastic link is assumed to be
%                             the i-th.
%       -complete_model[Default: false]: either true or false, if true(and 
%                                        dtheta non empty) the complete 
%                                        dynamic model will be derived
%   
% and outputs:
%   -M: The inertia matrix of the robot encoded by DHParams
%   -c: The coriolis and centrifugal coefficients of the robot encoded by
%       DHParams
%   -g: The gravity vector acting on the robot encoded by DHParams.
    
    %% Parsing input parameters
    expectedAxis = {'1', '2', '3', '-1', '-2', '-3'};
    n_dof = size(DHParams, 1);
    
    p = inputParser;
    addRequired(p, 'DHParams', @(x) (size(x,2) == 4));
    addRequired(p, 'com_pos', @(x) (size(x,2) == 3) && size(x,1) == n_dof);
    addRequired(p, 'q', @(x) (length(x) == n_dof));
    addRequired(p, 'dq', @(x) (length(x) == n_dof));
    addRequired(p, 'vertical_axis', ...
        @(x) any(validatestring(string(x), expectedAxis)));
    addOptional(p, 'com_mot_pos', [], ...
        @(x) (size(x, 2) == 3) && size(x,1) == n_dof);
    addOptional(p, 'rot_mot', zeros(n_dof, 3), ...
        @(x) (size(x, 2) == 3) && size(x,1) == n_dof);
    addOptional(p, 'reduction_ratios', ones(n_dof), ...
        @(x) (length(x) == n_dof));
    addOptional(p, 'theta', [], @(x) (length(x) == n_dof));
    addOptional(p, 'dtheta', [], @(x) (length(x) == n_dof));
    addOptional(p, 'complete_model', false, @(x) (isa(x, 'logical')));
    
    parse(p, DHParams, com_pos, q, dq, vertical_axis, varargin{:});
    
    com_mot_pos = p.Results.com_mot_pos;
    rot_mot = p.Results.rot_mot;
    reduction_ratios = p.Results.reduction_ratios;
    theta = p.Results.theta;
    dtheta = p.Results.dtheta;
    elastic = ~isempty(dtheta) && ~isempty(com_mot_pos);
    complete_model = p.Results.complete_model;
    
    %% Generating symbolic variables
    m = createSymbols('m_', 1, n_dof);                          % links' masses
    mr = createSymbols('m_r_', 1, n_dof);                       % rotors' masses
    I = [createSymbols('I_%xx', 1, n_dof).';                    % inertia coefficients of all the links
         createSymbols('I_%yy', 1, n_dof).'; 
         createSymbols('I_%zz', 1, n_dof).'];
    
    g_0 = createSymbols('g_', 0, 0);                            % gravity_acceleration absolute value
    g_trs = sym(zeros(1, 3));                                   % defining the gravity acceleration vector along the correct axis
    g_trs(abs(vertical_axis)) = -sign(vertical_axis)*g_0;
    
    if ~isempty(com_mot_pos) 
        mr = createSymbols('mr_', 1, n_dof);                    % motors' masses
        Im = [createSymbols('I_m%xx', 1, n_dof).';              % inertia matrix of all the motors
              createSymbols('I_m%yy', 1, n_dof).'; 
              createSymbols('I_m%zz', 1, n_dof).'];

        if elastic
            K = diag(createSymbols('K', 1, n_dof));             % Declaring symbolic variables of elasticity coefficients
        end
    end
    
    %% Generating always-required matrices
    Tl = kineticEnergyLinks(DHParams, com_pos, q, dq, m, I);                % Total kinetic energy of the robots' links
    M  = inertiaMatrixLinks(Tl, dq);                                        % Inertia matrix of the robot without elasticity and motors
    U = potentialEnergyLinks(DHParams, com_pos, q, m, g_trs);               % Total potential energy of the robot in the simplified model(no S)
    g = gravityVector(U, q);                                                % Gravity vector of the robot in the simplified model(no S)
    
    %% Considering motors' energies
    if ~isempty(com_mot_pos)
        Ur = potentialEnergyMotors(DHParams, com_mot_pos, q, mr, g_trs);
        gr = gravityVector(Ur, q);
        g = g + gr;
        
        if elastic                                                          % When we have elasticity we consider 2n variables.
            
            theta_m = theta.*reduction_ratios;                              % Applying reduction ratios
            dtheta_m = dtheta.*reduction_ratios;
            
            Tr = kineticEnergyMotors(DHParams, com_mot_pos, ...             % Total kinetic energy of the robots' motors w.r.t. dtheta
                                     rot_mot, q, dq, dtheta_m, mr, Im);
            
            [Mr, Bm, S] = inertiaMatrixMotors(Tr, dq, dtheta, ...
                                              complete_model);              % Inertia matrix of the robot's motors w.r.t. dtheta

            elastic_gravity_vector = gravityVectorElasticLinks(K, q, ...
                                                               theta_m);
            g = [g + elastic_gravity_vector; -elastic_gravity_vector];      % Adding gravity vectors due to elasticity
                 
            M = [M+Mr+S/Bm*S.' S; transpose(S) Bm];                         % Elasticity case inertia matrix
        else
            Tr = kineticEnergyMotors(DHParams, com_mot_pos, rot_mot, q, ... % Total kinetic energy of the robots' motors w.r.t. dtheta
                                     dq, dq.*reduction_ratios, mr, Im);
            [~, Bm, ~] = inertiaMatrixMotors(Tr, dq, dq, false);            % Inertia matrix of the robot's motors w.r.t. dq
            M = M + Bm;                                                     % Non-elastic case inertia matrix considering the motors
        end
    end
    
    if elastic
        c = coriolisCentrifugalVector(M, [q; theta], [dq; dtheta]);         % Coriolis and centrifugal forces of the elastic robot
    else
        c = coriolisCentrifugalVector(M, q, dq);                            % Coriolis and centrifugal forces of the non-elastic robot
    end
end