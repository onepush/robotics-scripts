clear; clc;

syms q_1 q_2 dq_1 dq_2 d_1 d_2 l_2 g_0 real

q = [q_1; q_2];
dq = [dq_1; dq_2];

params = [pi/2 0 q_1 0; 0 l_2 0 q_2];
com_pos = [0 q_1-d_1 0 ; d_2-l_2 0 0];

vertical_axis = -1;