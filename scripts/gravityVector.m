function g = gravityVector(U, q)
% g = gravityVector(U, q) takes as inputs:
%   -U: The expression of the total potential energy
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
% and outputs:
%   -g: The symbolic n-vector of the gravity vector resulting from the
%       total potential energy U.

    g = transpose(jacobian(U, q));

end