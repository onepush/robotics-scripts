function J_dls = dampedLeastSquare(J, singularity_distance)
% Jdls = dampedLeastSquare(J)
%   -J: a jacobian matrix(but actually any matrix)
%   -singularity_distance: the maximum distance to a singularity to set
%                          mu=0
% and outputs:
%   -Jdls: the damped least square solution of the task -> minimizing error
%          and velocity norm. Always introduces a little error but also
%          induces a robust behavior when crossing singularities.
% NOTE: This is still to be tested

    [U, S, V] = svd(J);
    
    mu = 0;
    min_sigma = min(diag(S));
    if min_sigma < singularity_distance
        mu = 1/min_sigma;
    end
    
    fprintf("mu set to %d\n", mu);
    
    [height, width] = size(S);
    S_dls = zeros(height, width);
    for index=1:min(height, width)
        sigma = S(index, index);
        S_dls(index, index) = sigma/(sigma^2+mu^2);
    end
    
    
    J_dls = V*S_dls*U.';
end