function [T, A] = dhMatrix(DHParams)
% T = DHMatrix(arrays) takes as inputs:
%   -DHParams: a nx4-matrix where the i-th row contains the following DH
%              parameters of link i: [alpha a d theta] - sorted like this!
% and outputs:
%   -T: the product of all the matrices corresponding to each vector of arrays
%   -A: Cell containing all the in-between matrices
% Remember that:
% cos(q1 + q2) = cos(q1)*cos(q2) - sin(q1)*sin(q2)
% sin(q1 + q2) = cos(q1)*sin(q2) + cos(q2)*sin(q1)
% making use of the simplify function these are converted automatically

    T = eye(4);
    nums = size(DHParams);
    
    A = cell(1,nums(1));
    
    for i = 1:nums(1)
        line = DHParams(i, :);
        R = [cos(line(4)) -cos(line(1))*sin(line(4)) sin(line(1))*sin(line(4)) line(2)*cos(line(4));
             sin(line(4)) cos(line(1))*cos(line(4)) -sin(line(1))*cos(line(4)) line(2)*sin(line(4));
             0 sin(line(1)) cos(line(1)) line(3);
             0 0 0 1;];
        A{i} = R;
        T = T * R;   
    end

    if isa(T, 'sym')
        T = simplify(T);
    end
end