function [Ur, Urs] = potentialEnergyMotors(DHParams, com_mot_pos, q, mr, g_trs)
% [Ur, Urs] = potentialEnergyMotors(DHParams, com_pos, q, vertical_axis) takes as inputs:
%   -DHParams: a n-vector of vectors composed like this: [alpha a d theta]
%   -com_mot_pos: a matrix of dimension nx3, where every i-th row is the 
%                 transpose of the position of the com of motor i w.r.t. 
%                 frame i-1
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
%   -mr: The n-vector containing the symbolic or numeric expressions of the
%       robot's motors masses.
%   -g_trs: The 3 row vector containing the g_0 acceleration only in the
%           vertical axis according to the base frame of the robot encoded
%           by DHParams.
% and outputs:
%   -Ur: The symbolic expression of the total potential energy of the
%       (open chained) manipulator encoded by DHParams.
%   -Urs: A n-vector containing all the symbolic expressions of the partial 
%        potential energies of the motort of the (open chained) manipulator
%        encoded by DHParams.

    n_dof = length(q);
    
    % Computing all the denavit-hartenberg matrices A_i
    [~, A] = dhMatrix(DHParams);
    
    % initialize useful variables
    Urs = sym(zeros(n_dof, 1));
    A_cumulative = eye(4);
    
    for i=1:n_dof
        pc_i = transpose(com_mot_pos(i, :));                                        % distance from Oi to CoMi as seen from frame i
        r_0_ci = simplify(A_cumulative(1:3, 4) + A_cumulative(1:3, 1:3)*pc_i);      % distance from Oi to CoMi as seen from frame 0
                                                                                    % the latter is computed as the position of Oi
                                                                                    % + the rotated(according to frame 0) pc_i
        Urs(i) = simplify(-mr(i)*g_trs*r_0_ci);                                     % potential energy Uri
        
        A_cumulative = A_cumulative * A{i};
    end
    
    % Computing the total potential energy of the manipulator
    Ur = simplify(sum(Urs));

end