function R = eulerRotation(sequence, angles)
% R = eulerRotation(sequence, angles) takes as inputs:
%   -sequence: a string which specifies the axes along which rotation
%              occurs
%   -angles: The radiants (or symbolics) of the three rotations
% and outputs:
%   -R: The desired rotation
% Euler rotations work about moving-axes
    
    if strlength(sequence) ~= 3
        disp("Sequence not valid, must be of lenght three.")
        return;
    end
    
    sequence = lower(char(sequence));
    if (sequence(2) == sequence(1) || sequence(2) == sequence(3))
        disp("Two consecutive rotation along the same axis are not valid.")
        return
    end
    
    R = elementaryRotationMatrix(sequence(1), angles(1)) * elementaryRotationMatrix(sequence(2), angles(2)) * elementaryRotationMatrix(sequence(3), angles(3));
    R = round(R, 8);
end