clear; clc;

syms q_1 q_2 q_3 dq_1 dq_2 dq_3 real
syms theta_1 theta_2 theta_3 dtheta_1 dtheta_2 dtheta_3 real
syms l1 l2 l3 d1 d2 d3 dm2 dm3 real
syms nr_1 nr_2 nr_3 real

q = [q_1; q_2; q_3];
dq = [dq_1; dq_2; dq_3];
dtheta = [dtheta_1; dtheta_2; dtheta_3];
theta = [theta_1; theta_2; theta_3];
reduction_ratios = [nr_1; nr_2; nr_3];

params = [
    pi/2 0 l1 q_1; 
    0 l2 0 q_2; 
    0 l3 0 q_3];

com_pos = [
    0 d1-l1 0; 
    d2-l2 0 0; 
    d3-l3 0 0];

com_mot_pos = [
    0 0 0;
    0 dm2-l1 0;
    dm3-l2 0 0];

rot_mot = [
    0 0 0;
    0 0 0;
    0 0 0];

vertical_axis = 3;

[M, c, g] = eulerLagrangeDynamicModel(params, com_pos, q, dq, vertical_axis, ...
           'com_mot_pos', com_mot_pos, 'rot_mot', rot_mot, ...
           'complete_model', true, 'dtheta', dtheta, ...
           'theta', theta, 'reduction_ratios', reduction_ratios);
