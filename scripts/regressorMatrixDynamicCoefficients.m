function Y = regressorMatrixDynamicCoefficients(tau, expr_a)
% Y = regressorMatrixDynamicCoefficients(tau, expr_a) takes as inputs:
%   -tau: The expression of the torques
%   -expr_a: Vector of expressions of selected dynamic coefficients a
% and outputs:
%   -Y: Regressor matrix such that: Y*a = tau_a, where a = [a1, ..., an]
%       and tau_a = subs(tau, expr_a, a)
    
    % Gathering dimensions
    n_a = length(expr_a);
    n_dof = length(tau);
    
    tau_a = tau;
    % Initializing variables and Y
    a = createSymbols('a%', 1, n_a);
    for i=1:n_a
        % subs is not very robust, better feed it an expression at a time
        tau_a = subs(tau_a, expr_a(i), a(i));
    end
    
    Y = sym(zeros(n_dof, n_a));
    
    for i = 1:n_dof
        for j = 1:n_a
            % Gathering factors of tau_a(i) w.r.t. a(j)
            tmp = coeffs(tau_a(i), a(j), 'All');
            
            if length(tmp) > 2
                % This case should be further investigated
                disp("Length greater than 2!");
                return
            elseif length(tmp) == 1
                % No element is multiplying a(j) keep A(i, j) = 0
                continue;
            else
                Y(i, j) = simplify(tmp(1));
            end
        end
    end
    
    disp("If the following is a vector of 0s then Y is probably correct, otherwise try to address the issue collecting the tau or doing it by hand");
    disp("Also, remember to pass expr_a in a descending order of complexity");
    res = simplify(Y*expr_a - tau)
end
