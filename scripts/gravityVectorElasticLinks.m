function ge = gravityVectorElasticLinks(K, q, theta)
% ge = gravityVectorElasticLinks(inputArg1,inputArg2) takes as inputs:
%   -K: The nxn diagonal matrix with the diagonal formed by the elastic
%       coefficients of the links.
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
%   -theta: the n-vector containing the symbolic expressions of the robot 
%           motors' position variables
% and outputs:
%   -ge: The gravity vectors dued to elasticity in the links.

    ge = K*(q-theta);
end

