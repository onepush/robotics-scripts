function [theta, r] = getThetaR(R)
% [theta, r] = getThetaR(R) takes as inputs:
%   -R: A rotation matrix, tipycally a 3x3 matrix
% and outputs:
%   -theta: The radiants the rotation has been performed of
%   -r: The vector the rotation has been performed about
% Remember to use eval(symbolic function) to use this function
    
    if isa(R, 'sym')
        disp("R must by non-symbolic matrix")
        theta = -1; r = -1;
        return
    end
    
    risp = input("You can use either the positive or the negative sin(theta), this will correspond to 2 different solutions, which one you prefer? (pos, neg)\n", "s");
    
    if risp == "pos"
        theta = atan2(sqrt((R(1,2)-R(2,1))^2 + (R(1,3)-R(3,1))^2 + (R(2,3)-R(3,2))^2), R(1,1)+R(2,2)+R(3,3)-1);
    else
        theta = atan2(-sqrt((R(1,2)-R(2,1))^2 + (R(1,3)-R(3,1))^2 + (R(2,3)-R(3,2))^2), R(1,1)+R(2,2)+R(3,3)-1);
    end
        
    if (abs(sin(theta)) >= 1e-8)
        r = [R(3,2)-R(2,3) R(1,3)-R(3,1) R(2,1)-R(1,2)] ./ (2*sin(theta));
        
    elseif theta == 0
            disp('Theta = 0, rotation axis is not defined, hence there is not a given solution');
            r = -1;
    else
        fprintf("Theta is %.2f (should be either +/- pi), hence we are in a singular case -> sin(theta) == 0\n", theta);
        
        r = [sqrt((R(1,1)+1)/2) sqrt((R(2,2)+1)/2) sqrt((R(3,3)+1)/2)];
        
        for x = 1:3
            for y = 1:3
                for z = 1:3
                    if (abs(r(1)*r(2)*2 - R(1,2)) <= 0.0001 && abs(r(1)*r(3)*2 - R(1,3)) <= 0.0001 && abs(r(2)*r(3)*2 - R(2,3)) <= 0.0001)
                        disp("Solution found, but remember that also -r is a solution")
                        return
                    end
                    r(3) = -r(3);
                end
                r(2) = -r(2);
            end
            r(1) = -r(1);
        end
        
        disp("Solution not found")
    end
end
        
    
  