function tau_trs = newtonEuler(dhParams, com_pos, q, dq, d2q, g_trs, I)
%FORWARDRECURSION Summary of this function goes here
%   Detailed explanation goes here
    n_dof = length(q);
    
    % Computing all the denavit-hartenberg matrices A_i
    [~, A] = dhMatrix(dhParams);
                 
    % Building a list of n_dof sigmas
    % The sigma i is 0 if the joint_i is revolute
    % and it is 1 if the joint_i is prismatic
    sigmas = zeros(1, n_dof);
    for index = 1:n_dof
        el = dhParams(index, end);
        if ~isa(el, 'sym') || ~ismember(el, q)
            sigmas(index) = 1;
        end
    end
    
    z_i = [0; 0; 1];
    w_trs = sym(zeros(n_dof, 3));
    dw_trs = sym(zeros(n_dof, 3));
    a = -g_trs;
    a_com = sym(zeros(n_dof+1, 3));
    
    for i=1:n_dof
        R_trs = transpose(A{i}(1:3, 1:3));
        w_trs(i, :) = transpose(R_trs*(w_trs(i, :).' + (1-sigmas(i))*dq(i)*z_i));
        dw_trs(i, :) = transpose(R_trs*(dw_trs(i, :).' + (1-sigmas(i))*(d2q(i)*z_i + dq(i)*cross(w_trs(i, :).', z_i))));
        r = simplify(R_trs*A{i}(1:3, 4));                                                       % The distance from O_i-1 to O_i as seen in frame i
        a = R_trs*(a + sigmas(i)*d2q(i)*z_i) + sigmas(i)*(cross(2*dq(i)*w_trs(i, :).', R_trs*z_i)) + cross(dw_trs(i, :).', r) + cross(w_trs(i, :), cross(w_trs(i, :).', r));
        a_com(i, :) = a + cross(dw_trs(i, :).', com_pos(i, :)) + cross(w_trs(i, :).', cross(w_trs(i, :), com_pos(i, :)));
    end
    
    f = zeros(3, 1);
    R = eye(3);
    mu_trs = sym(zeros(n_dof+1, 3));
    tau_trs = sym(zeros(n_dof, 3));
    
    for j=n_dof:1
        f = R*f + m*a_com(j);
        r = simplify(R*A{j}(1:3, 4));
        J = diag(I(:, j));
        mu_trs(j, :) = transpose(R*mu_trs(j+1, :).' + cross(R*f, com_pos(j, :)) - cross(f, r+com_pos(i, :).') + J*dw_trs(j, :).' + cross(w_trs(j, :).', J*w_trs(j, :).'));
        tau_trs(j, :) = sigmas(j)*f.'*R.'*z_in + (1-sigmas(j))*mu_trs(j, :)*R.'*z_in;
        R = A{j}(1:3, 1:3);
    end
end

