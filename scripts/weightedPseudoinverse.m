function Jw = weightedPseudoinverse(J, W)
% Jw = weightedPseudoinverse(J, W) takes as inputs:
%   -J: a Jacobian matrix
%   -W: a symmetric positive definite matrix which will contain the weights
% and outputs:
%   -Jw: The weighted, according to W, pseudoinverse of J.
% NOTE: This is still to be tested

    if abs(det(J)) < 1e-5
        Jw = W\J.'/(J*W\J.');
    else
        sqrt_W = sqrt(W);
        Jw = sqrt_W\pinv(J/sqrt_W);
    end