function symbols = createSymbols(name, min, max, workspace)
% symbols = createSymbols(name, number) takes as input
%   -name: The name of the symbols you want to create, 
%          in particular should be splittable in:
%          pre + '%' + post, where pre and post can be ''
%          and the script will initialise 
%          pre+string(min)+post ... pre+string(max)+post
%          however if name does not contain % the indices will be just
%          placed at the end, generating name+string(index)
%   -min: The starting index to create a variable of
%   -max: The last index to create a variable of
%   -workspace[Default: 'base']: Can be either 'caller' or 'base'
% and outputs:
%   -symbols: a (max-min+1)-vector formed by the created symbols 
%             initialised in the desired workspace.
    
    %% workspace is an optional parameter
    if ~exist('workspace', 'var')
        workspace = 'base';
    end
    
    %% splitting name in pre and post
    % if % is not contained in the name string pre will be the whole name
    % otherwise pre will be the preceding part and post the following part
    % only the first % will be considered
    if ~contains(name, '%')
        pre = name;
        post = "";
    else
        idx = strfind(name, '%');
        pre = name(1:idx-1);
        post = name(idx+1:end);
    end

    %% creating the symbols
    symbols = sym(zeros(max-min+1, 1));
    for i=min:max
        symbols(i-min+1) = sym(strcat(pre, string(i), post));
    end
    syms(symbols, 'real');

    %% Passing the symbols to the desired workspace
    for index=min:max
        assignin(workspace, strcat(pre, string(index), post), symbols(index-min+1))
    end
    
end