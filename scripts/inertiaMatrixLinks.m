function M = inertiaMatrixLinks(T, dq)
% M = inertiaMatrixLinks(T, dq) takes as inputs:
%   -T: The total kinetic energy of a (open-chained) manipulators' robot's
%       motors
%   -dq: The n-vector containing the symbolic expressions of the robot's
%        joints' velocity variables
% and outputs:
%   -M: The symbolic expression of the inertia matrix
    
    n_dof = length(dq);
    T = 2*T; % The formula is T = 1/2*q.'*M*q, so I "remove the 1/2"
    M = sym(zeros(n_dof));
    
    % Introduced 2 approaches to deal with this problem
    % The second is a little more elegant and, probably, less
    % computationally expensive.
    
    %for i = 1:n_dof
    %    factors = coeffs(T, dq(i), 'All');
    %    
    %    if length(factors) > 3 % should have up to dqi^2
    %        disp("There are more than 3 coefficients resulting from coeffs(T, dq(i)) with i=", i)
    %    end
    %    
    %    M(i, i) = factors(1);
    %    
    %    for j = (i+1):n_dof
    %        tmp = coeffs(factors(2), dq(j), 'All');
    %        
    %        tot = 0;
    %        for k = length(tmp)-1:1
    %           tot = tot + tmp(k)*dq(j)^(k-1);
    %        end
    %        tot = tot/2; % halving symmetric terms
    %        
    %        M(i, j) = tot;
    %        M(j, i) = tot;
    %    end
    %end
    
    for i = 1:n_dof
        % differentiating w.r.t. dq_i and dq_j to obtain the element m_ij
        M(i,i) = diff(T, dq(i), 2) / 2; % Would introduce a factor 2 which is not desired
        temp = diff(T, dq(i));
        for j = (i+1):n_dof
            coeff = diff(temp, dq(j))/2; % Halving symmetric contributions
            M(i, j) = coeff;
            M(j, i) = coeff;
        end
    end
    
    M = simplify(M);
end
