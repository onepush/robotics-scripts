function v_out = rodriguez(r, theta, v)
% v_out = rodriguez(r, theta, v) takes as inputs:
%   -r: The vector we want to rotate about
%   -theta: the radiants we want to rotate of
%   -v: the vector we want to apply the rotation
% and outputs:
%   -v_out: the rotated vector
% The Rodriguez formula is used to find the vector v' corresponding to v rotated about the r vector of theta radiants
% Remember to use eval(symbolic function) to use this function
    
    if class(r) == "sym" || class(theta) == "sym" || class(v) == "sym"
        disp("Symbols are not allowed.\nUsage: rodriguez(r = [a, b, c], theta = radiants, v = [a, b, c]\n")
        return
    end
    
    v_out = v.*cos(theta) + cross(r,v).*sin(theta) + ((1 - cos(theta)) * (r * transpose(v))).* r;

end