clear; clc;

syms q_1 q_2 dq_1 dq_2 theta_1 theta_2 dtheta_1 dtheta_2 ...
    l1 l2 d1 d2 dm2

q = [q_1; q_2];
dq = [dq_1; dq_2];
theta = [theta_1; theta_2];
dtheta = [dtheta_1; dtheta_2];
reduction_ratios = [1;1];

params = [0 l1 0 q_1; 0 l2 0 q_2];
com_l = [d1-l1 0 0; d2-l2 0 0];

com_m = [
    0 0 0;
    dm2 0 0];

vertical_axis = 3;

Tr = kineticEnergyMotors(params, com_m, dtheta, dq);

%[M, c, g] = eulerLagrangeDynamicModel(params, com_pos, q, dq, vertical_axis, ...
%            'com_mot_pos', com_mot_pos, ...
%            'complete_model', true, 'dtheta', dtheta, ...
%            'theta', theta, 'reduction_ratios', reduction_ratios);