function [Tr, Trs] = kineticEnergyMotors(DHParams, com_mot_pos, rot_mot, q, dq, dtheta, mr, Im)
% [Tr, Trs] = kineticEnergyMotors(DHParams, com_pos_mot, q, dq, dtheta, 
%                                 mr, Im) takes as inputs:
%   -DHParams: a nx4-matrix where the i-th row contains the following DH
%              parameters of link i: [alpha a d theta] - sorted like this!
%   -com_mot_pos: a matrix of dimension nx3, where every i-th row is the 
%                 transpose of the position of the com of motor i w.r.t. 
%                 frame i-1.
%   -rot_mot: nx3-matrix with the i-th row containing 3 elements:
%             alpha_i, beta_i and gamma_i encoding an euler rotation
%             through the 'xyz' sequence which represents the orientation
%             of motor i w.r.t. to frame i-1
%   -q: The n-vector containing the symbolic expressions of the robot's
%       joints' position variables
%   -dq: The n-vector containing the symbolic expressions of the robot's
%        joints' velocity variables
%   -dtheta: The n-vector containing the symbolic expressions of the robot'
%            motors velocities
%   -mr: The n-vector containing the symbolic or numeric expressions of the
%       robot's motors masses.
%   -I: The 3xn-matrix containing the symbolic or numeric expressions of
%       the robot's motors' inertia components. 
%       The i-th column contains the 3 diagonal inertias of motor i.
% and outputs:
%   -Tr: The symbolic expression of the total kinetic energy of the motors
%        of the (open chained) manipulator encoded by DHParams.
%   -Trs: A n-vector containing all the symbolic expressions of the partial 
%         kinetic energies of the motors of the (open chained) manipulator 
%         encoded by DHParams.
    
    n_dof = length(q);
      
    % Computing all the denavit-hartenberg matrices A_i
    [~, A] = dhMatrix(DHParams);
    z_i = [0; 0; 1]; % the z_i axis vector expressed according to frame i
    
    % Building a list of n_dof sigmas
    % The sigma i is 0 if the joint_i is revolute
    % and it is 1 if the joint_i is prismatic
    sigmas = zeros(1, n_dof);
    for index = 1:n_dof
        el = DHParams(index, end);
        if ~isa(el, 'sym') || ~ismember(el, q)
            sigmas(index) = 1;
        end
    end
    
    % initial angular/translational velocities
    v = zeros(3, 1);
    w = zeros(3, 1);
    
    Trs = sym(zeros(n_dof, 1)); % vector of partial kinetic energies of the motors
    
    for i = 1:n_dof
        R_mot_trs = transpose(eulerRotation('xyz', rot_mot(i, :)));
        r = com_mot_pos(i, :).';
        w_m = simplify(R_mot_trs*(w + (1-sigmas(i))*dtheta(i)*z_i));
        v_m = simplify(R_mot_trs*(v + sigmas(i)*dtheta(i)*z_i + cross(w, r)));
        
        Trs(i) = simplify(0.5*(transpose(v_m)*mr(i)*v_m + transpose(w_m)*diag(Im(:, i))*w_m));
        
        R_trs = transpose(A{i}(1:3, 1:3));
        w = simplify(w + (1-sigmas(i))*dq(i)*z_i);
        v = simplify(R_trs*(v + sigmas(i)*dq(i)*z_i + cross(w, A{i}(1:3, 4))));
        w = simplify(R_trs*w);
    end
    
    Tr = simplify(sum(Trs)); % basically T += Ti for all i
end


