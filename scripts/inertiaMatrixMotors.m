function [Mr, Bm, S] = inertiaMatrixMotors(Tr, dq, dtheta, complete_model)
% [Mr, Bm, S] = inertiaMatrixMotors(T, dq, dtheta, complete_model)
% takes as inputs:
%   -Tr: The total kinetic energy of a (open-chained) manipulators' robot's
%        motors
%   -dq: The n-vector containing the symbolic expressions of the robot's
%        joints' velocity variables
%   -dtheta: the n-vector containing the symbolic expressions of the robot 
%            motors' velocity variables.
%   -complete_model: logical value which indicates whether we want the
%                    complete or the simplified(Mr = S = 0) models
% and outputs:
%   -Mr: The inertia component felt by the links due to the motors
%   -Bm: The symbolic expression of the inertia matrix of the motors
%   -S: The matrix, significative only if complete_model is true, which 
%       models the couplings between links and motors
    
    n_dof = length(dq);
    Mr = sym(zeros(n_dof));
    S = sym(zeros(n_dof));
    Bm = sym(zeros(n_dof));
    
    Tr = 2*Tr; % The formula is Tr = 1/2*q.'*Mr*q, so I "remove the 1/2"
    
    if complete_model
        for i = 1:n_dof
            Mr(i, i) = diff(Tr, dq(i), 2)/2; % Would introduce a factor 2 which is not desired
            Bm(i, i) = diff(Tr, dtheta(i), 2)/2; % Would introduce a factor 2 which is not desired
            temp = diff(Tr, dq(i));
            for j = (i+1):n_dof
                coeff = diff(temp, dq(j))/2; % Halving symmetric contributions
                Mr(i, j) = coeff;
                Mr(j, i) = coeff;
                S(i, j) = diff(temp, dtheta(j));
            end
        end
        Mr = Mr - S/Bm*S.';
    else
        for i = 1:n_dof
            temp = diff(Tr, dtheta(i));
            for j = i:n_dof
                coeff = diff(temp, dtheta(j))/2; % Halving symmetric contributions
                Bm(i, j) = coeff;
                Bm(j, i) = coeff;
            end
        end
    end
end
